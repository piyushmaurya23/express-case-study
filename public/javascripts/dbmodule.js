var mongojs = require("mongojs");
var db = mongojs("databaseURL", ["todo"]);

exports.addTodo = function(req, res) {
  console.log("req", req.body);
  db.todo.save(
    {
      content: req.body.content,
      date: new Date().toLocaleDateString(),
      isDone: false
    },
    function(err, saved) {
      if (err) {
        console.log(err);
        res.render("error", { error: err });
      } else {
        console.log(saved);
        res.redirect("/todo");
      }
    }
  );
};

exports.markDone = function(req, res) {
  db.todo.findAndModify(
    {
      query: { _id: mongojs.ObjectId(req.params.id) },
      update: {
        $set: { isDone: true, date: new Date().toLocaleDateString() }
      },
      new: true
    },
    function(err, doc) {
      if (err) {
        console.log(err);
        res.render("error", { error: err });
      } else {
        res.redirect("/todo");
        console.log(doc);
      }
    }
  );
};

exports.getTodo = function(req, res) {
  db.todo.find({ isDone: false }, function(err, docs) {
    if (err) {
      console.log(err);
      res.render("error", { error: err });
    } else {
      res.render("todo", { data: docs });
    }
  });
};

exports.getDoneTodo = function(req, res) {
  db.todo.find({ isDone: true }, { _id: 0 }, function(err, docs) {
    if (err) {
      console.log(err);
      res.render("error", { error: err });
    } else {
      res.render("done", { data: docs });
    }
  });
};

exports.deleteTodo = function(req, res) {
  db.todo.remove({ _id: mongojs.ObjectId(req.params.id) }, function(err, doc) {
    if (err) {
      console.log(err);
      res.render("error", { error: err });
    } else {
      res.redirect("/todo");
      console.log(doc);
    }
  });
};

exports.markAllDone = function(req, res) {
  db.todo.update(
    { isDone: false },
    {
      $set: { isDone: true, date: new Date().toLocaleDateString() }
    },
    { multi: true },
    function(err, doc) {
      if (err) {
        console.log(err);
        res.render("error", { error: err });
      } else {
        res.redirect("/todo");
        console.log(doc);
      }
    }
  );
};
