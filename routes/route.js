var express = require("express");
var router = express.Router();

var dbmodule = require("../public/javascripts/dbmodule");

/* GET home page. */
router.get("/", function(req, res, next) {
  res.render("index", {});
});

router.get("/todo", dbmodule.getTodo);

router.post("/todo", dbmodule.addTodo);

router.get("/done", dbmodule.getDoneTodo);

router.get("/markdone/:id", dbmodule.markDone);

router.get("/delete/:id", dbmodule.deleteTodo);

router.get("/markalldone", dbmodule.markAllDone);

module.exports = router;
